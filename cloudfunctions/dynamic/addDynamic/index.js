//增加动态
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database() //数据库
const addDynamic = async function (params) {
  const wxContext = cloud.getWXContext()
  const resultData = await db.collection('dynamic').add({
    data: {
      _openid: wxContext.OPENID,
      message: params.message,
      images: params.images,
      dynamicTag: params.dynamicTag,
      createTime:new Date().getTime()
    }
  })
  return resultData
}
module.exports = addDynamic